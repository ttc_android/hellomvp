package com.example.thuandq.hellomvp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.thuandq.hellomvp.listener.LoginPresenter;
import com.example.thuandq.hellomvp.listener.LoginView;
import com.example.thuandq.hellomvp.presenter.LoginPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginView, View.OnClickListener {

    @BindView(R.id.editUserName)
    EditText username;
    @BindView(R.id.editPassword)
    EditText password;
    @BindView(R.id.processBar)
    ProgressBar progressBar;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        presenter = new LoginPresenterImpl(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLogin) {
            presenter.validateCredentials(username.getText().toString(), password.getText().toString());
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void setUsernameError(String error) {
        username.setError(error);
    }
    @Override
    public void setPasswordError(String error) {
        password.setError(error);
    }

    @Override
    public void navigateToHome() {
        Toast.makeText(this, "go to home", Toast.LENGTH_LONG).show();
    }
}
