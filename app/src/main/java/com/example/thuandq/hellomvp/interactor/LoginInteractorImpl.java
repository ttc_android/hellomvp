package com.example.thuandq.hellomvp.interactor;

import android.os.Handler;
import android.text.TextUtils;

import com.example.thuandq.hellomvp.listener.LoginInteractor;
import com.example.thuandq.hellomvp.listener.OnLoginFinishedListener;

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener listener) {
        // Mock login. I'm creating a handler to delay the answer a couple of seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean error = false;
                if (TextUtils.isEmpty(username) || username.length() < 6) {
                    listener.onUsernameError("Tên phải lớn hơn 6 ký tự");
                    error = true;
                }
                if (TextUtils.isEmpty(password)|| password.length() < 6) {
                    listener.onPasswordError("Mật khẩu phải lớn hơn 6 ký tự");
                    error = true;
                }
                if (!error) {
                    listener.onSuccess();
                }
            }
        }, 1000);
    }
}