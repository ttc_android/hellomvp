package com.example.thuandq.hellomvp.listener;

public interface LoginInteractor {
    public void login(String username, String password, OnLoginFinishedListener listener);
}
