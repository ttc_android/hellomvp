package com.example.thuandq.hellomvp.listener;

public interface LoginPresenter {
    public void validateCredentials(String username, String password);
}
