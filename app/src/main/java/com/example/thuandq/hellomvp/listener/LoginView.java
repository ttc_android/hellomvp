package com.example.thuandq.hellomvp.listener;

public interface LoginView {
    public void showProgress();

    public void hideProgress();

    public void setUsernameError(String error);

    public void setPasswordError(String error);

    public void navigateToHome();
}
