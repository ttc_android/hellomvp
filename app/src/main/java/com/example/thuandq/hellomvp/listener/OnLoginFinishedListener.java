package com.example.thuandq.hellomvp.listener;

public interface OnLoginFinishedListener {
    public void onUsernameError(String error);

    public void onPasswordError(String error);

    public void onSuccess();
}
