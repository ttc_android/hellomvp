package com.example.thuandq.hellomvp.presenter;

import com.example.thuandq.hellomvp.interactor.LoginInteractorImpl;
import com.example.thuandq.hellomvp.listener.LoginInteractor;
import com.example.thuandq.hellomvp.listener.LoginPresenter;
import com.example.thuandq.hellomvp.listener.LoginView;
import com.example.thuandq.hellomvp.listener.OnLoginFinishedListener;

public class LoginPresenterImpl implements LoginPresenter, OnLoginFinishedListener {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void validateCredentials(String username, String password) {
        loginView.showProgress();
        loginInteractor.login(username, password, this);
    }

    @Override
    public void onUsernameError(String error) {
        loginView.setUsernameError(error);
        loginView.hideProgress();
    }

    @Override
    public void onPasswordError(String error) {
        loginView.setPasswordError(error);
        loginView.hideProgress();
    }

    @Override
    public void onSuccess() {
        loginView.navigateToHome();
        loginView.hideProgress();
    }
}